SHELL=/bin/bash

up:
	docker-compose up -d

certs:
	bash -c 'openssl req -x509 -newkey rsa:2048 -keyout ~/.dinghy/certs/www.chat.dev.key \
    	-out ~/.dinghy/certs/www.chat.dev.crt -days 365 -nodes \
    	-subj "/C=PL/ST=Mazowieckie/L=Warsaw/O=Markiewicz/OU=Org/CN=*.chat.dev" \
    	-config <(cat /etc/ssl/openssl.cnf <(printf "[SAN]\nsubjectAltName=DNS:*.chat.dev")) \
		-reqexts SAN -extensions SAN'

init:
	docker-compose run --rm --no-deps php-upstream bash -c "\
		composer install --prefer-dist --no-suggest \
	"

test-unit:
	docker-compose run --rm php-upstream bash -c "\
		bin/phpunit \
	"

test-dredd:
	echo "will be done l8r"

migrate:
	docker-compose run --rm php-upstream bash -c "\
		bin/console doctrine:migrations:migrate -n --env=dev ;\
		DATABASE_URL=mysql://root:secret_password@mysql:3306/test bin/console doctrine:migrations:migrate -n --env=test "

fixtures-dev:
	docker-compose run --rm php-upstream bin/console doctrine:fixtures:load -n
