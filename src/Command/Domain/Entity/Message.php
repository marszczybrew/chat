<?php
declare(strict_types=1);

namespace App\Command\Domain\Entity;

use App\Command\Domain\Identity\UserId;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity()
 * @ORM\Table(name="messages")
 */
class Message
{
    /**
     * @var \Ramsey\Uuid\UuidInterface
     * @ORM\Column(type="uuid")
     * @ORM\Id()
     */
    private $id;
    /**
     * @var User
     * @ORM\Column(type="uuid")
     */
    private $sender;
    /**
     * @var User
     * @ORM\Column(type="uuid")
     */
    private $recipient;
    /**
     * @var string
     * @ORM\Column(type="text")
     */
    private $content;
    /**
     * @var \DateTimeImmutable
     * @ORM\Column(type="datetime_immutable")
     */
    private $sentAt;

    private function __construct(UuidInterface $from, UuidInterface $to, string $content)
    {
        $this->id = Uuid::uuid4();
        $this->sender = $from;
        $this->recipient = $to;
        $this->content = $content;
        $this->sentAt = new \DateTimeImmutable();
    }

    public static function write(User $from, User $to, string $content)
    {
        return new self($from->getId(), $to->getId(), $content);
    }
}
