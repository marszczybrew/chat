<?php
declare(strict_types=1);

namespace App\Command\Domain\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity()
 * @ORM\Table(name="users")
 */
class User implements UserInterface
{
    /**
     * @var \Ramsey\Uuid\UuidInterface
     * @ORM\Column(type="uuid")
     * @ORM\Id()
     */
    private $id;
    /**
     * @var string
     * @ORM\Column(type="string", unique=true)
     */
    private $name;
    /**
     * @var string
     * @ORM\Column(type="string", unique=true)
     */
    private $token;

    private function __construct(string $name)
    {
        $this->id = Uuid::uuid4();
        $this->name = $name;
        $this->token = bin2hex(random_bytes(8));
    }

    public static function register(string $name)
    {
        return new self($name);
    }

    #region Symfony
    public function getRoles()
    {
        return ['ROLE_USER'];
    }

    public function getPassword()
    {
    }

    public function getSalt()
    {
    }

    public function getUsername()
    {
        return $this->name;
    }

    public function eraseCredentials()
    {
    }

    #endregion Symfony

    public function updateToken(string $token)
    {
        $this->token = $token;
    }

    public function getId(): UuidInterface
    {
        return $this->id;
    }
}
