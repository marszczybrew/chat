<?php


namespace App\Command\Domain\Exception;

use Throwable;

class UsernameTaken extends \Exception
{
    public function __construct(string $username)
    {
        parent::__construct($username . " is alrady taken");
    }
}
