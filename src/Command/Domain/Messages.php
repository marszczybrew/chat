<?php
declare(strict_types=1);

namespace App\Command\Domain;

use App\Command\Domain\Entity\Message;
use App\Command\Domain\Exception\UserNotFound;

interface Messages
{
    /**
     * @throws UserNotFound
     */
    public function send(Message $message): void;
}
