<?php


namespace App\Command\Domain;

use App\Command\Domain\Entity\User;
use App\Command\Domain\Exception\UsernameTaken;
use App\Command\Domain\Exception\UserNotFound;

interface Users
{
    /**
     * @throws UsernameTaken
     */
    public function save(User $user): void;

    /**
     * @throws UserNotFound
     */
    public function findByName(string $name): User;

    /**
     * @throws UserNotFound
     */
    public function findById(string $id): User;
}
