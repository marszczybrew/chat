<?php
declare(strict_types=1);

namespace App\Command\Infrastructure;

use App\Command\Domain\Entity\Message;
use App\Command\Domain\Messages;
use Doctrine\ORM\EntityManagerInterface;

class OrmMessages implements Messages
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function send(Message $message): void
    {
        $this->em->persist($message);
        $this->em->flush();
    }
}
