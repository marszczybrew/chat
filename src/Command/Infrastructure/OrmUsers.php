<?php
declare(strict_types=1);

namespace App\Command\Infrastructure;

use App\Command\Domain\Entity\User;
use App\Command\Domain\Exception\UsernameTaken;
use App\Command\Domain\Exception\UserNotFound;
use App\Command\Domain\Users;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;

class OrmUsers implements Users
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function save(User $user): void
    {
        try {
            $this->em->persist($user);
            $this->em->flush();
        } catch (UniqueConstraintViolationException $e) {
            throw new UsernameTaken($user->getUsername());
        }
    }

    public function findByName(string $name): User
    {
        $user = $this->em->getRepository(User::class)
            ->findOneBy(['name' => $name]);
        if ($user === null) {
            throw new UserNotFound();
        }

        return $user;
    }

    public function findById(string $id): User
    {
        $user = $this->em->getRepository(User::class)
            ->findOneBy(['id' => $id]);
        if ($user === null) {
            throw new UserNotFound();
        }

        return $user;
    }
}
