<?php
declare(strict_types=1);

namespace App\Bundle\Controller;

use App\Command\Domain\Entity\Message;
use App\Command\Domain\Exception\UserNotFound;
use App\Command\Domain\Messages;
use App\Command\Domain\Users;
use App\Query\Application\FetchMessages;
use App\Query\Application\FetchMessagesHandler;
use App\Query\Exception\ConversationNotFound;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route(path="/users/{recipientId}/messages")
 */
class MessagesController extends Controller
{
    /**
     * @Route(methods={"POST"})
     */
    public function sendMessage(string $recipientId, Request $request, Users $users, Messages $messages)
    {
        try {

            $user      = $this->getUser();
            $recipient = $users->findById($recipientId);
            $content   = $request->get("content");
            $message   = Message::write($user, $recipient, $content);

            $messages->send($message);

            return new JsonResponse(null, JsonResponse::HTTP_NO_CONTENT);
        } catch (UserNotFound $e) {
            return new JsonResponse(['message' => $e->getMessage(), JsonResponse::HTTP_BAD_REQUEST]);
        }
    }

    /**
     * @Route(methods={"GET"})
     */
    public function viewMessages(string $recipientId, FetchMessagesHandler $handler)
    {
        try {
            $loggedUserId = (string)$this->getUser()->getId();
            $query        = FetchMessages::between($loggedUserId, $recipientId);
            $messages     = $handler->handle($query);

            return new JsonResponse($messages);
        } catch (ConversationNotFound $e) {
            return new JsonResponse(['message' => $e->getMessage()], JsonResponse::HTTP_NOT_FOUND);
        }
    }
}
