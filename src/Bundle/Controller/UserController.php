<?php


namespace App\Bundle\Controller;

use App\Command\Domain\Entity\User;
use App\Command\Domain\Exception\UsernameTaken;
use App\Command\Domain\Exception\UserNotFound;
use App\Command\Domain\Users;
use App\Query\Application\FetchUsers;
use App\Query\Application\FetchUsersHandler;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route(path="/users")
 */
class UserController extends Controller
{
    /**
     * @Route(path="", methods={"POST"})
     */
    public function registerAction(Request $request, Users $users)
    {
        try {
            $name = $request->get("name");
            $user = User::register($name);
            $users->save($user);

            return new JsonResponse();
        } catch (UsernameTaken $e) {
            return new JsonResponse(['message' => $e->getMessage()], JsonResponse::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @Route(methods={"GET"})
     */
    public function listUsersAction(Request $request, FetchUsersHandler $handler): JsonResponse
    {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $query = FetchUsers::withoutTokens();
        } else {
            // to allow easier user switching (no need to go into DB to see tokens) we display all tokens to anonymous users
            $query = FetchUsers::withAuthTokens();
        }
        $users = $handler->handle($query);

        return new JsonResponse($users);
    }

    /**
     * @Route(path="/token", methods={"POST"})
     */
    public function updateTokenAction(Request $request, Users $users)
    {
        try {
            $name  = $request->get("name");
            $token = $request->get("token");

            $user = $users->findByName($name);
            $user->updateToken($token);

            $users->save($user);

            return new JsonResponse();
        } catch (UserNotFound $e) {
            return new JsonResponse(['message' => $e->getMessage()], JsonResponse::HTTP_NOT_FOUND);
        }
    }
}
