<?php
declare(strict_types=1);

namespace App\Bundle\Listener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class JsonRequestBodyConverter implements EventSubscriberInterface
{

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::REQUEST => [
                ['transformRequest', 0],
            ],
        ];
    }

    public function transformRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();
        if (!$request->headers->has('Content-Type')) {
            return;
        }
        if (0 === stripos($request->headers->get('Content-Type'), 'application/json')) {
            $data = json_decode($request->getContent(), true);
            $request->request->replace(is_array($data) ? $data : []);
        }
    }
}
