<?php
declare(strict_types=1);

namespace App\DataFixtures;

use App\Command\Domain\Entity\Message;
use App\Command\Domain\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class ChatFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $unicorn = User::register('unicorn');
        $dog     = User::register('dog');
        $cat     = User::register('cat');

        foreach ([$unicorn, $dog, $cat] as $user) {
            $manager->persist($user);
        }

        $messages = [
            Message::write($cat, $dog, "Hi there doge, wanna talk?"),
            Message::write($dog, $cat, "Sure, but after I eat your tail"),
            Message::write($cat, $dog, "No thanks, I'll go talk with unicorn"),
            Message::write($cat, $unicorn, "Hey unicorn, sup?"),
            Message::write($unicorn, $cat, "I have so much work, wish someone could help me"),
            Message::write($cat, $unicorn, "Oh, can I help you somehow?"),
            Message::write($unicorn, $cat, "Can you write PHP?"),
            Message::write($unicorn, $dog, "Hi doge, are you looking for work?"),
        ];

        foreach ($messages as $message) {
            $manager->persist($message);
        }

        $manager->flush();
    }
}
