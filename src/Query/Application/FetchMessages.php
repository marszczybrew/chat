<?php
declare(strict_types=1);

namespace App\Query\Application;

class FetchMessages
{
    private $one;
    private $another;

    private function __construct(string $oneUser, string $otherUser)
    {
        $this->one     = $oneUser;
        $this->another = $otherUser;
    }

    public static function between(string $oneUser, string $otherUser): self
    {
        return new self($oneUser, $otherUser);
    }

    public function getIds(): array
    {
        return [$this->one, $this->another];
    }
}
