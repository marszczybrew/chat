<?php
declare(strict_types=1);

namespace App\Query\Application;

class CheckUserExists
{
    private $userId;

    private function __construct(string $id)
    {
        $this->userId = $id;
    }

    public static function byId(string $id)
    {
        return new self($id);
    }

    public function id()
    {
        return $this->userId;
    }
}
