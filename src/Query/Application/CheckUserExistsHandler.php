<?php
declare(strict_types=1);

namespace App\Query\Application;

use Doctrine\DBAL\Connection;

class CheckUserExistsHandler
{
    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function handle(CheckUserExists $query): bool
    {
        $sql    = "SELECT COUNT(1) FROM users WHERE id = :id";
        $params = ['id' => $query->id()];

        return $this->connection->executeQuery($sql, $params)->fetchColumn() == 1;
    }
}
