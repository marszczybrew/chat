<?php
declare(strict_types=1);

namespace App\Query\Application;

use App\Query\Exception\ConversationNotFound;
use App\Query\Infrastructure\Collection\MessagesCollection;
use App\Query\Infrastructure\DTO\Message;
use Doctrine\DBAL\Connection;

class FetchMessagesHandler
{
    private $connection;

    private $userChecker;

    public function __construct(Connection $connection, CheckUserExistsHandler $userChecker)
    {
        $this->connection  = $connection;
        $this->userChecker = $userChecker;
    }

    /**
     * @throws ConversationNotFound
     */
    public function handle(FetchMessages $query): MessagesCollection
    {
        $userIds = $query->getIds();

        foreach ($userIds as $user) {
            if (!$this->userChecker->handle(CheckUserExists::byId($user))) {
                throw new ConversationNotFound();
            }
        }

        $collection = new MessagesCollection();

        foreach ($this->fetchMessages($userIds) as $rawMessage) {
            $collection->add($this->hydrate($rawMessage));
        }

        return $collection;
    }

    private function fetchMessages(array $userIds): \Generator
    {
        $sql    = "SELECT sender, content, sent_at FROM messages WHERE sender IN (:users) AND recipient IN (:users) ORDER BY sent_at DESC";
        $params = ['users' => $userIds];
        $types  = ['users' => Connection::PARAM_STR_ARRAY];
        $stmt   = $this->connection->executeQuery($sql, $params, $types);

        while (($row = $stmt->fetch(\PDO::FETCH_ASSOC)) !== false) {
            yield $row;
        }
    }

    private function hydrate(array $message): Message
    {
        return new Message($message['sender'], $message['content'], new \DateTimeImmutable($message['sent_at']));
    }
}
