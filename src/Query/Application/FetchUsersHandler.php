<?php
declare(strict_types=1);

namespace App\Query\Application;

use App\Query\Infrastructure\Collection\UsersCollection;
use App\Query\Infrastructure\DTO\User;
use Doctrine\DBAL\Connection;

class FetchUsersHandler
{
    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function handle(FetchUsers $query): UsersCollection
    {
        $includeTokens = $query->includeTokens();

        $users = new UsersCollection();
        foreach ($this->fetchUsers() as $user) {
            $users->add($this->hydrate($user, $includeTokens));
        }

        return $users;
    }

    private function fetchUsers(): \Generator
    {
        $sql  = "SELECT id, name, token FROM users";
        $stmt = $this->connection->executeQuery($sql);

        while (($row = $stmt->fetch(\PDO::FETCH_ASSOC)) !== false) {
            yield $row;
        }
    }

    private function hydrate(array $userRow, bool $includeToken): User
    {
        return new User($userRow['id'], $userRow['name'], $includeToken ? $userRow['token'] : null);
    }
}
