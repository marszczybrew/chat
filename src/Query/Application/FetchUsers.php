<?php
declare(strict_types=1);

namespace App\Query\Application;

class FetchUsers
{
    private $includeTokens;

    private function __construct(bool $includeTokens)
    {
        $this->includeTokens = $includeTokens;
    }

    public static function withoutTokens()
    {
        return new self(false);
    }

    public static function withAuthTokens()
    {
        return new self(true);
    }

    public function includeTokens(): bool
    {
        return $this->includeTokens;
    }
}
