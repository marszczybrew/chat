<?php
declare(strict_types=1);

namespace App\Query\Exception;

class ConversationNotFound extends \Exception
{
    public function __construct()
    {
        parent::__construct("Conversation not found");
    }
}
