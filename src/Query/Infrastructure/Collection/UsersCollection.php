<?php
declare(strict_types=1);

namespace App\Query\Infrastructure\Collection;

use App\Query\Infrastructure\DTO\User;

class UsersCollection implements \IteratorAggregate, \JsonSerializable
{
    private $users;

    public function __construct()
    {
        $this->users = [];
    }

    public function add(User $message)
    {
        $this->users[] = $message;
    }

    public function getIterator()
    {
        return new \ArrayIterator($this->users);
    }

    public function jsonSerialize()
    {
        return [
            'users' => $this->users,
        ];
    }
}
