<?php
declare(strict_types=1);

namespace App\Query\Infrastructure\Collection;

use App\Query\Infrastructure\DTO\Message;

class MessagesCollection implements \IteratorAggregate, \JsonSerializable
{
    private $messages;

    public function __construct()
    {
        $this->messages = [];
    }

    public function add(Message $message)
    {
        $this->messages[] = $message;
    }

    public function getIterator()
    {
        return new \ArrayIterator($this->messages);
    }

    public function jsonSerialize()
    {
        return [
            'messages' => $this->messages,
        ];
    }
}
