<?php
declare(strict_types=1);

namespace App\Query\Infrastructure\DTO;

class User implements \JsonSerializable
{
    private $id;
    private $name;
    private $token;

    public function __construct(string $id, string $name, string $token = null)
    {
        $this->id    = $id;
        $this->name  = $name;
        $this->token = $token;
    }

    public function id(): string
    {
        return $this->id;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function token(): ?string
    {
        return $this->token;
    }

    public function jsonSerialize()
    {
        return [
            'id'    => $this->id,
            'name'  => $this->name,
            'token' => $this->token,
        ];
    }
}
