<?php


namespace App\Query\Infrastructure\DTO;

class Message implements \JsonSerializable
{
    private $sender;
    private $content;
    private $sentAt;

    public function __construct(string $sender, string $content, \DateTimeImmutable $sentAt)
    {
        $this->sender  = $sender;
        $this->content = $content;
        $this->sentAt  = $sentAt;
    }

    public function sender()
    {
        return $this->sender;
    }

    public function content()
    {
        return $this->content;
    }

    public function sentAt()
    {
        return $this->sentAt;
    }

    public function jsonSerialize()
    {
        return [
            'sender'  => $this->sender,
            'content' => $this->content,
            'sentAt'  => $this->sentAt->format("Y-m-d H:i:s"),
        ];
    }
}
