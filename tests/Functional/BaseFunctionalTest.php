<?php
declare(strict_types=1);

namespace App\Tests\Functional;

use Doctrine\DBAL\Connection;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

abstract class BaseFunctionalTest extends WebTestCase
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var string|null
     */
    private $token;

    public function setUp()
    {
        $this->client     = static::createClient();
        $this->connection = static::$kernel->getContainer()->get("doctrine.dbal.default_connection");
    }

    public function tearDown()
    {
        $this->connection->exec("TRUNCATE messages; TRUNCATE users;");
    }

    public function loginAs(string $username): void
    {
        $this->token = $this->getTokenForUser($username);
    }

    protected function getTokenForUser(string $username): string
    {
        $sql = "SELECT token FROM users WHERE name = :username";

        return $this->connection->executeQuery($sql, ['username' => $username])->fetchColumn();
    }

    protected function assertSecondUserWasntCreated()
    {
        $response = $this->client->getResponse();
        $this->assertEquals(400, $response->getStatusCode());
        $this->assertContains('foo', $response->getContent());
    }

    protected function getUserFromApi(string $name)
    {
        $this->client->request('GET', '/users', ['token' => $this->token]);
        $users    = json_decode($this->client->getResponse()->getContent(), true);
        $matching = array_filter($users['users'], function ($user) use ($name) {
            return $user['name'] === $name;
        });
        // usernames should be unique
        $this->assertCount(1, $matching);

        return array_pop($matching);
    }

    protected function createUser(string $name)
    {
        $this->client->request('POST', '/users', ['name' => $name]);
    }

    protected function sendMessage(string $recipient, string $content, string $recipientId = null)
    {
        $recipientId = $recipientId ?? $this->getIdForUser($recipient);
        $this->client->request('POST', "/users/$recipientId/messages",
                               ['content' => $content, 'token' => $this->token]);
    }

    protected function getIdForUser(string $username): string
    {
        $sql = "SELECT id FROM users WHERE name = :username";

        return $this->connection->executeQuery($sql, ['username' => $username])->fetchColumn();
    }

    protected function getMessages(string $username, string $userId = null)
    {
        $userId = $userId ?? $this->getIdForUser($username);
        $this->client->request('GET', "/users/$userId/messages", ['token' => $this->token]);

        return json_decode($this->client->getResponse()->getContent(), true)['messages'];
    }

    protected function assertConversationDoesntExist(string $userId)
    {
        $this->client->request('GET', "/users/$userId/messages", ['token' => $this->token]);
        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());
    }
}
