<?php
declare(strict_types=1);

namespace App\Tests\Functional;

use Ramsey\Uuid\Uuid;

class MessagesTest extends BaseFunctionalTest
{

    /**
     * @test
     */
    public function iCanSendMessages()
    {
        $this->createUser('foobar');
        $this->createUser('foobar_friend');
        $this->loginAs('foobar');

        $this->sendMessage('foobar_friend', "Hey foobar!");
        $this->assertCount(1, $this->getMessages('foobar_friend'));

        $this->sendMessage('foobar_friend', "Are you there?");
        $this->assertCount(2, $this->getMessages('foobar_friend'));
    }

    /**
     * @test
     */
    public function bothUsersCanSeeTheSameMessages()
    {
        $this->createUser('cat');
        $this->createUser('unicorn');

        $this->loginAs('cat');
        $this->sendMessage('unicorn', 'Hi Unicorn!');
        $catPointOfView = $this->getMessages('unicorn');

        $this->loginAs('unicorn');
        $unicornPointOfView = $this->getMessages('cat');

        $this->assertEquals($catPointOfView, $unicornPointOfView);
    }

    /**
     * @test
     */
    public function iCantSendMessagesToNonexistentUsers()
    {
        $this->createUser('dog');
        $this->loginAs('dog');

        $nonexistentRecipientId = Uuid::uuid4()->toString();

        $this->sendMessage('', 'This message shouldn\'t be sent', $nonexistentRecipientId);
        $this->assertConversationDoesntExist($nonexistentRecipientId);
    }
}
