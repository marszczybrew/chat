<?php
declare(strict_types=1);

namespace App\Tests\Functional;

class UserTest extends BaseFunctionalTest
{

    /**
     * @test
     */
    public function iCanRegister()
    {
        $this->createUser('foo');
        $this->assertNotEquals(null, $this->getUserFromApi('foo'));
    }

    /**
     * @test
     * @depends iCanRegister
     */
    public function usernameHasToBeUnique()
    {
        $this->createUser('foo');
        $this->createUser('foo');
        $this->assertSecondUserWasntCreated();
    }

    /**
     * @test
     */
    public function usersDontSeeOtherUsersTokens()
    {
        $this->createUser('foo');
        $this->createUser('bar');
        $this->loginAs('foo');
        $user = $this->getUserFromApi('foo');

        $this->assertEquals(null, $user['token']);
        $this->assertEquals('foo', $user['name']);
    }

    /**
     * @test
     */
    public function anonymousUsersCanSeeTokens()
    {
        $this->createUser('unicorn');
        $unicorn = $this->getUserFromApi('unicorn');

        // 16 = standard token length
        $this->assertEquals(16, strlen($unicorn['token']));
    }
}
