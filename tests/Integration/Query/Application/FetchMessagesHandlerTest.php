<?php
declare(strict_types=1);

namespace App\Tests\Integration\Query\Application;

use App\Query\Application\FetchMessages;
use App\Query\Application\FetchMessagesHandler;
use App\Query\Exception\ConversationNotFound;
use App\Query\Infrastructure\Collection\MessagesCollection;
use Doctrine\DBAL\Connection;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class FetchMessagesHandlerTest extends KernelTestCase
{
    /**
     * @var FetchMessagesHandler
     */
    private $handler;
    /**
     * @var Connection
     */
    private $connection;


    public function setUp()
    {
        static::bootKernel();
        $this->handler    = static::$kernel->getContainer()->get("test.App\Query\Application\FetchMessagesHandler");
        $this->connection = static::$kernel->getContainer()->get("doctrine.dbal.default_connection");
    }

    public function tearDown()
    {
        $this->connection->exec("TRUNCATE messages");
    }

    /**
     * @test
     */
    public function itThrowsWhenUsersDontExist()
    {
        $this->expectException(ConversationNotFound::class);
        $this->handler->handle(FetchMessages::between("some-nonexistent", 'user-ids'));
    }

    /**
     * @test
     */
    public function itReturnsEmptyCollectionWhenNoMessages()
    {
        $one    = $this->generateUser();
        $two    = $this->generateUser();
        $result = $this->handler->handle(FetchMessages::between($one, $two));
        $this->assertInstanceOf(MessagesCollection::class, $result);
        $this->assertCount(0, $result);
    }

    /**
     * @test
     */
    public function itReturnsMessagesWrittenByBothUsers()
    {
        $one = $this->generateUser();
        $two = $this->generateUser();
        $this->insertMessage($one, $two, "First message");
        $this->insertMessage($two, $one, "First reply");

        $result = $this->handler->handle(FetchMessages::between($one, $two));
        $this->assertInstanceOf(MessagesCollection::class, $result);
        $this->assertCount(2, $result);
    }

    private function generateUser(): string
    {
        $id = Uuid::uuid4()->toString();
        $this->connection->insert('users', [
            'id'    => $id,
            'name'  => bin2hex(random_bytes(2)),
            'token' => bin2hex(random_bytes(6)),
        ]);

        return $id;
    }

    private function insertMessage(
        string $userOne,
        string $userTwo,
        string $content,
        \DateTimeImmutable $time = null): void
    {
        $this->connection->insert(
            "messages",
            [
                'id'        => Uuid::uuid4(),
                'sender'    => $userOne,
                'recipient' => $userTwo,
                'content'   => $content,
                'sent_at'   => $time ?? new \DateTimeImmutable(),
            ],
            [
                'uuid',
                'string',
                'string',
                'string',
                'datetime',
            ]
        );
    }

    /**
     * @test
     */
    public function itDoesntIncludeOtherMessages()
    {
        $one   = $this->generateUser();
        $two   = $this->generateUser();
        $three = $this->generateUser();

        $this->insertMessage($one, $two, "One to Two");
        $this->insertMessage($two, $three, "Two to Three");
        $this->insertMessage($three, $two, "Three to Two");
        $this->insertMessage($one, $three, "One to Three");
        $this->insertMessage($two, $one, "Two to One");

        $result = $this->handler->handle(FetchMessages::between($one, $two));
        $this->assertInstanceOf(MessagesCollection::class, $result);
        $this->assertCount(2, $result);
    }
}
