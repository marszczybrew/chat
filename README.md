# Chat

## Installation

> Prerequisites: Docker, docker-compose  

### HTTP proxy

I'm using [dory](https://github.com/FreedomBen/dory) to avoid exposing nginx port on my PC. If you don't have it you can uncomment those few lines in `docker-compose.yml`, but you'll have to send `curl` to `localhost:10080` instead of `www.chat.dev`.

### App initialization

```bash
make init
make up
make migrate
```

There are some fixtures you can load if you don't want to populate the DB yourself. Run this command to do so:

```bash
make fixtures-dev
```

## Copy'n'pastes

I recommend adding [` | jq`](https://stedolan.github.io/jq/) after each curl for nice formatting.

### Register

```bash
curl -X POST -F 'name=unicorn' http://www.chat.dev/users
```

### List users (with tokens)

```bash
curl http://www.chat.dev/users
```

### Send a message

```bash
curl -X POST -H'X-Auth: <token>' -F'content=Hi there unicorn!' http://www.chat.dev/users/<uuid>/messages
```

### View messages

```bash
curl -H'X-Auth: <token>' http://www.chat.dev/users/<uuid>/messages
```

## API contract

Documentation can be viewed in `apiary.apib` file or [online](https://chatty1.docs.apiary.io/#).
